#!/bin/bash

OUT=dl1-pr2-deliverable.zip

test -f "$OUT" && rm -i "$OUT"

find -mindepth 1 -maxdepth 1 \
    -not -name DATA \
    -not -name __pycache__ \
    -not -name lib \
    -exec zip -r "$OUT" {} + | grep -v .git/

echo "Generated file as:"
echo "$OUT   (size $(du -sh "$OUT" | awk '{print $1}')) (contains $(unzip -l "$OUT" | wc -l) entries)"

