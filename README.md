# Handwritten digit recognition with Pytorch

## Subject

The goals of this project was to fine-tune a deep learning model train on
handwritten digits.  
If you want to open this repository into a python executable environement (binder) please follow the link bellow :  
[https://mybinder.org/v2/gl/William-maillard%2Fm1_dl_handwritten_digit_recognition/main?labpath=README.md](https://mybinder.org/v2/gl/William-maillard%2Fm1_dl_handwritten_digit_recognition/main?labpath=README.md)


Below are all the steps and try I tested in order to improve some caracteristics of the model such as the executing time or the accuracy.

## T1.1

Some suggested commands:

```sh
./run-train.sh DATASET_TINY=True
./run-train.sh DATASET_TINY=True EVAL_MOD=20 EVAL_INIT=False
./run-train.sh
```

Questions:

- How long does it take to do 1 epoch on the tiny dataset? and what is the name of the log file?
  - environ 2 secondes
  - Log: trace-2023-03-27T103219_local_tiny
- How long does it take to do 1 epoch on the full dataset?
  - environ 18 secondes
  - Log: trace-2023-03-21T143549_local
- Do you observe overfitting on the tiny dataset?
  - Yes at the end the model has a 100% accuracy on the training set and a 52% accuracy on the test set
  - Log: trace-2023-03-27T103219_local_tiny
- Do you observe overfitting on the normal dataset?
  - Yes it also reach 100% accuracy on the trainning set but not on the test set.
  - Log: trace-2023-03-21T143549_local

## T1.2

Some suggested commands:

```sh
./run-train.sh DET_TIATASNY=True
./run-train.sh DATASET_TINY=True FORCE_CPU=True
```

Questions:

- How long does it take to do 1 epoch on the tiny dataset? with GPU? with CPU?
  - environ 0.25 seconde avec GPU
  - Log: trace-2023-03-21T14 20 03 TINY_lyse_cloud
- How long does it take to do 1 epoch on the full dataset?
  - environ 0.62 secondes avec GPU
  - Log: trace-2023-03-21T14 24 26_lyse_cloud

## Experiments

Instructions:

- Fill the forms below, copy the "Experiment" section as many times as necessary.
- You must also give the log files with your report.

Observations of the data set:

- there is some noice in the data (like a 2 that looks like a 4, or a 6 that looks like a 8)*
- with have inbalance data for classes: class 1 has 3 picture, and class M has 250.
- the data have different resolution. (edit: transform_custom in helpers normalize the pictures).

Environment local:

- CPU: Intel(R) Core(TM) i7-10510U CPU @ 1.80GHz, 4 cores
- GPU: None
- RAM: 16Go
- OS: windows 11

-----------------1

### Change activations functions to ReLU

- Id: use-relu
- Log: trace-2023-03-27T173928_reLU
- Reference: BASE
- Environment: local
- Changes:
  - change the two activations to ReLU
- Observations:
  - the gap btw the training and test accuracy is the praticaly the same as the base model
  - The accuracy is increasing faster than with the base model and the loss is decreasing faster.
  - the loss reach a slowest value 0.021497
  - As expected the time of eecution is slower of the base model (because value are not clamp btw -1 and 1 but 0 and +infiny)
- Ideas:
  - changing the activation function to ReLU increase a little the accuracy of our model but it didn't solve
    the problem of overfitting and the execution is slower.
    So let's try to increase the drop out to have less overfitting.

-----------------2

### increase drop out to 0.5

- Id: increase-dropout
- Log: trace-2023-03-27T182249_increase-dropout
- Reference: use-relu
- Environment: local
- Changes:
  - increase the drop out from 0.3 to 0.5
- Observations:
  - the rate of loss is similare to the base model
  - the accuracy on the test set is much better on the first bach but at the end it is still overfitting, even if the accuracy us slightly better.
- Ideas:
  - try to add a drop out of 0.5 to the first layer

-----------------3

### change optimiser from adam to LBFGBS

- Id: LBFGS
- Log: trace-2023-03-27T185038_LBFGS
- Reference: BASE
- Environment: local
- Changes:
  - change the optimisert from adam to LBFGS, wich is working on the wall bach to compute the loss (might be slower?), but can be more accurate because use the previous computed gradient at each step.
- Observations:
  - greater average loss at the beginning
  - the accuracy is a litter worse than the base model at the beginning
  - the model is faster of 0.3s on average
  - the accuracy on the test set is better than the base model of 5%
  - still overfiting on the train set
- Ideas:
  - maybe used it with the best result obtain with dropout, because with LBFGS the model seems to be faster and more accurate than with adam.

-----------------4

### Add a drop out of 0.5 to the first layer

- Id: add-dropout-1
- Log: trace-2023-03-27T191030_add_dropout_first_layer
- Environment: local
- Changes:
  - add a drop out of 0.5 to the first layer
- Observations:
  - it reduces the execution time by at least 1s
  - as espected the accuracy on test an train at the begginning are worst than the base case
  - the accuracy increasing rate is slower.
  - it no longer overfit the model but the accuracy is worse than the base model.
- Ideas:
  - add an over dropout to the last layer to see if its improve again the execution speed.
  - reduce the drop out to 0.3

-----------------5

### add drop out of 0.5 to the third layer

- Id: add-dropout-3
- Log: trace-2023-03-27T191618_add_dropout_3
- Reference: add-dropout-1
- Environment: local
- Changes:
- add a drop out of 0.5 to the third layer
- Observations:
- a higher average loss by 0.5
- the accuracy is higher  than the base model
    at the end for the test model (45.3% against 36.8%) but also for the train model (89.4% against 79.1%)
- it is smaller of 0.1 second on average  than the base model
- Ideas:
  - try to decrease all drop out value to 0.3 in order to gain overfitting (and since we have a dropout at each level, it might prevent over-fitting)

-----------------6

### reduce all dropout values to 0.3 in order to gain accuracy

- Id: reduce-dropout
- Log: trace-2023-03-27T204858_reduce_dropout
- Reference: add-dropout-3
- Environment: local
- Changes:
  - reduce all dropout to 0.3
- Observations:
  - faster than the base model by 0.3 second on average
  - the model is approximatively 4% more accurate than the base model, but it overfit with 100% accuracy on the train set.
- Ideas:
  - change the optimizer of the model

-----------------7

### use of LBFGS optimizer instead of adam

- Id: mix-dropout-LBFGS
- Log: trace-2023-03-27T211116_error_lbfgs_with_drop_out
- Reference: add-dropout-3
- Environment: local
- Changes:
  - LBFGS optimizer instead of adam
- Observations:
  - raise an error missing parameter in LBFGS.step()

-----------------8

### use of SGD optimizer which work like LBFGS but using random small mini-batch

- Id: opti-sgd
- Log: trace-2023-03-27T211352_SGD
- Reference: add-dropout-3
- Environment: local
- Changes:
  - use SGD optimizer
- Observations:
  - O.3s faster in average
  - But at the end 24% less accurate on the test set than the base model, thus it is not a good choice.
- Ideas:
  - instead of changing the optimiser to gain accuracy, let's add some layers in order to learn a more complex model.
  - increase the number of neurone in the model

-----------------9

### increase output second layer

- Id: inc-out-l2
- Log: trace-2023-03-27T230109_inc_feat
- Reference: add-dropout-3
- Environment: local
- Changes:
  - chage the output of the second layer from 512 to 1024, and repercute the change in the nn.Linear layer.
- Observations:
  - slower by 3s
  - less accurate than the model base.

-----------------10

### add a convolutionnal layer

- Id: add-conv
- Log: trace-2023-03-27T232612_add_conv
- Reference: add-dropout-3
- Environment: local
- Changes:
  - add a convolutionnal layer to end with a 1x1 img size :

    ```py
    model = nn.Sequential(
        # the input is a 1 channel image
        nn.BatchNorm2d(1),
        nn.Conv2d(1, 128, kernel_size=5), # here 32-4=28
        nn.BatchNorm2d(128),
        nn.Dropout(0.5),
        nn.MaxPool2d(4), # here 28//4=7 the img dmensions
        Act(),
        nn.Conv2d(128, 512, kernel_size=2), # here 7-1=6
        nn.BatchNorm2d(512),
        nn.Dropout(0.5),
        nn.MaxPool2d(2), # here 6 // 2 = 3

        # !!!!! here
        Act(),
        nn.Conv2d(512, 512, kernel_size=2), # here 3-1=2
        nn.BatchNorm2d(512),
        nn.Dropout(0.5),
        nn.MaxPool2d(2), # here 2 // 2 = 1
        # ----------------------

        Act(),
        nn.Flatten(),
        nn.Dropout(0.5),
        nn.Linear(
            512 * ((((32-4)//4-1)//2-1)//2)**2, # computed from the conv and pool above,
            len(classes)                        #  **2 bc height==width.
        )
        # the output is len(classes)-sized logits
    )
    ```

- Observations:
  - the time of execution is longer by 2s (because of the added computation of the new layer)
  - the accuracy on the test set is bad, 7.5% on average
  - thus it is not good to reduce the size of the image to 1x1
- Ideas:
- try to increase the stride of the kernel at the beginning, maybe able to better detect space information like edges and corners.

-----------------11

### increase the kernel stride

- Id: add-conv
- Log: trace-2023-03-27T235147_ker-stride
- Reference: add-dropout-3
- Environment: local
- Changes:
  - nicrease the first convolution kernel stride from 1 to 3
  - comment the 2 last nn.MaxPool2d
  - Here the updated model:

    ```py
    model = nn.Sequential(

        # the input is a 1 channel image

        nn.BatchNorm2d(1),
        nn.Conv2d(1, 128, kernel_size=5, stride=3), # here (32-5)//3+1=10
        nn.BatchNorm2d(128),
        nn.Dropout(0.5),
        nn.MaxPool2d(2), # here 10//2=5 the img dmensions
        Act(),
        nn.Conv2d(128, 512, kernel_size=2), # here 5-1=4
        nn.BatchNorm2d(512),
        nn.Dropout(0.5),
        #nn.MaxPool2d(2),
        Act(),
        nn.Conv2d(512, 512, kernel_size=2), # here 4-1=3
        nn.BatchNorm2d(512),
        nn.Dropout(0.5),
        #nn.MaxPool2d(2),
        Act(),
        nn.Flatten(),
        nn.Dropout(0.5),
        nn.Linear(
            512 * (((((32-5)//3+1)//2-1)-1))**2, # computed from the conv and pool above, **2 bc height==width.
            len(classes)
        )
        # the output is len(classes)-sized logits
    )
    ```

- Observations:
  - it is faster by 2s than the base model
  - it has better accuracy on the test model with 39.6%, and don't overfit the trainning sample
-Ideas:
  - maybe not enought deep ? Try an other convolution layer at the end.
  - maybe to much drop out in the network, that prevent the program to learn well, thus try to remove dropout at certain position
   (I firstly think at the beginning, thus the model will not always use the same first node to base its decision on => force to use all the images info it can get
    but over positions can be consider)

-----------------12

### add a second convolution layer

- Id: add-conv-2
- Log: trace-2023-03-27T230109_add_conv_2
- Reference: add-conv
- Environment: local
- Changes:

    ```py
    model = nn.Sequential(
        # the input is a 1 channel image
        nn.BatchNorm2d(1),
        nn.Conv2d(1, 128, kernel_size=5, stride=3), # here (32-5)//3+1=10
        nn.BatchNorm2d(128),
        nn.Dropout(0.5),
        nn.MaxPool2d(2), # here 10//2=5 the img dmensions
        Act(),
        nn.Conv2d(128, 512, kernel_size=2), # here 5-1=4
        nn.BatchNorm2d(512),
        nn.Dropout(0.5),
        Act(),
        nn.Conv2d(512, 512, kernel_size=2), # here 4-1=3
        nn.BatchNorm2d(512),
        nn.Dropout(0.5),
        #nn.MaxPool2d(2),
        Act(),
        nn.Conv2d(512, 256, kernel_size=2), # here 3-1=2
        nn.BatchNorm2d(256),
        nn.Dropout(0.5),
        Act(),
        nn.Flatten(),
        nn.Dropout(0.5),
        nn.Linear(
            256 * (((((32-5)//3+1)//2-1)-1-1))**2, # computed from the conv and pool above, **2 bc height==width.
            len(classes)
        )
        # the output is len(classes)-sized logits
    )
    ```

- Observations:
  - slower by 0.2s
  - less accurate (maybe because of to much dropout)
  - greater loss 1.6 against 0.7

----------------13

### remove dropout

- Id: rm-dropout
- Log: trace-2023-03-28T002123_rm_dpt
- Reference: add-conv-2
- Environment: local
- Changes:
  - remove all dropout except for the first layer
- Observations:
  - on average the execution time don't change
  - the accuracy is better by 2% on the test set
  - but overfit the trainning set (100%)
- Idea:
- what append if we reverse this experience (only kept the last drop out) ?
   maybe reinforce the learning from the whole network (by taking alternative path at the end) and increase accuracy ?
 -instead of removing dropout reduce it gradualy in the network.

-----------------14

### remove dropout reverse

- Id: rm-dropout-reverse
- Log: trace-2023-03-28T002924_rm_dpt_rvs
- Reference: rm-dropout
- Environment: local
- Changes:
  - remove all dropout except for the last layer
- Observations:
  - overfitting
- Idea:
- next: decreasing the dropout with the deph in the network

-----------------15

### decreasing dropout

- Id: decrease-dropout
- Log: trace-2023-03-28T003454_decrease_drop_out
- Reference: add-conv-2
- Environment: local
- Changes:
  - firs drop out 0.5, decrease until 0.1 for the last layer.
- Observations:
  - same execution time
  - better accuracy on test set 38.7%, not overfitting

-----------------16

### add params to convolution layers

- Id: add-params
- Log: trace-2023-03-28T081652_add_params
- Reference: decrease-dropout
- Environment: local
- Changes:
  - add padding to first and second layer associated respectively
    with a stride and dilation, in order to get more sparse information.
  - here the modified model:

    ```py
    model = nn.Sequential(
        # the input is a 1 channel image
        nn.BatchNorm2d(1),
        nn.Conv2d(1, 128, kernel_size=5, stride=3, padding=3), # here (32+2*3-5)//3+1=12
        nn.BatchNorm2d(128),
        nn.Dropout(0.5),
        Act(),
        nn.Conv2d(128, 512, kernel_size=2, padding=4, dilation=3), # here 17
        nn.BatchNorm2d(512),
        nn.Dropout(0.4),
        Act(),
        nn.Conv2d(512, 512, kernel_size=2, ), # here 17-1=16
        nn.BatchNorm2d(512),
        nn.Dropout(0.3),
        nn.MaxPool2d(2), # 8
        Act(),
        nn.Conv2d(512, 256, kernel_size=4), # here 8-3=5
        nn.BatchNorm2d(256),
        nn.Dropout(0.2),
        Act(),
        nn.Flatten(),
        nn.Dropout(0.1),
        nn.Linear(256* 5**2, # computed from the conv and pool above,**2 bc height==width.
            len(classes)
        )
        # the output is len(classes)-sized logits
    )
    ```

- Observations:
  - the execution time is slower by 10s on average (and 20s some times) than the base case
  - the loss is low at the end: 0.2
  - we reach a great accuracy on the test model 49.1%, but it  overfit a little with 99.8% accuracy on trainning set
- Idea:
- reduce the nb of features to reduce the computation time,
   by giving more feature for larger kernel (in order to get information from large kernel)

-----------------17

### change the feature size of each layers depending of their kernel size

- Id: tune-features
- Log: trace-2023-03-28T085835_tune_features
- Reference: add-params
- Environment: local
- Changes:
  - The updated model:
  
    ```py
      model = nn.Sequential(
          # the input is a 1 channel image
          nn.BatchNorm2d(1),
          nn.Conv2d(1, 512, kernel_size=5, stride=3, padding=3), # here (32+2*3-5)//3+1=12
          nn.BatchNorm2d(512),
          nn.Dropout(0.5),
          Act(),
          nn.Conv2d(512, 64, kernel_size=2, padding=4, dilation=3), # here 17
          nn.BatchNorm2d(64),
          nn.Dropout(0.4),
          Act(),
          nn.Conv2d(64, 64, kernel_size=2), # here 17-1=16
          nn.BatchNorm2d(64),
          nn.Dropout(0.3),
          nn.MaxPool2d(2), # 8
          Act(),
          nn.Conv2d(64, 256, kernel_size=4), # here 8-3=5
          nn.BatchNorm2d(256),
          nn.Dropout(0.2),
          Act(),
          nn.Flatten(),
          nn.Dropout(0.1),
          nn.Linear(256* 5**2, # computed from the conv and pool above,**2 bc height==width.
              len(classes)
          )
          # the output is len(classes)-sized logits
      )
    ```

- Observations:
  - more faster then the base model (2.8s instead of 10-20s)
  - the los is higher by 0.1
  - the accuracy is lower by 5% on both test an train (less overfit)

-----------------18

### change optimizer from adma to adagrad

- Id: -opti-adagrad
- Log: trace-2023-03-28T090814_adagrad
- Reference: tune-features
- Environment: local
- Changes:
  - change the activation function
- Observations:
  - 11.3 accuracy, not very good
Idea:
  - maybe mix the activation function, use ReLU for 2 first layers in order to get a maximum information
    and use tanh for the 2 laast ones to clamp the result btw -1 and 1.

-----------------19

### mix ReLU and tanh

- Id: mix-act
- Log: trace-2023-03-28T093518_mix_act
- Reference: tune-features
- Environment: local
- Changes:
  - mix activations funtion:
    -ReLU for 2 first layers
    -Tanh for the 2 last ones
- Observations:
  - slower on average by 0.4s
  - loss decrease slowerly
  - better accuracy on the test set by 3% than the base model
  - but increase the accuracy of the trainning set by 4, which lead to overfit

-----------------20

### change the loss function

- Id: nllloss
- Log: trace-2023-03-28T093518_nllloss
- Reference: mix-act
- Environment: local
- Changes:
  - change the loss function from CrossEntropy to NLLLoss
- Observations:
  - less than 10% accuracy, not a good choixe, very huge negative loss.
