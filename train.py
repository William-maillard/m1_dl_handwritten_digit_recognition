
print("Importing dependencies (preloading)")

import sys
if len(sys.argv) < 2 or sys.argv[1] != 'DO-RUN':
    print("\nPlease use the ./run-train.sh script to run this file.\n\n")
    sys.exit()

import torch
from torch import nn
from torch.utils.data import DataLoader
from torchvision import datasets, transforms

print("Starting")
from helper import (
    load_custom_dataset,
    evaluate_accuracy, train_one_epoch, SimpleTimer,
    classes,
    digest_command_line,
)

#######
DATASET_TINY = False # use a small fraction of the dataset (can be used for faster debugging)
DATASET_PREVIEW_IMAGES = 0 # image count (per set), use 0 to disable preview
EVAL_MOD = 1 # evaluate every n epochs (modulus)
EVAL_INIT = True # whether to evaluate at initialization
QUIET = False # whether to reduce the printing during training loops
BATCH_SIZE = 64 # the batch size (used for the dataloaders)
EPOCHS = 100 # number of epochs before (automatically) stopping
LR = 1e-4 # Learning rate
SEED = 0 # force random seed by setting to non-zero
FORCE_CPU = False # do not use GPU even if available

# feel free to add any PARAMETER here instead of hardcoded values (these parameters will be automatically accepted at the command line)

# we accept command line arguments like EVAL_INIT=False
digest_command_line(sys.argv[2:], globals())

reload_autosave = False
use_gpu = False if FORCE_CPU else torch.cuda.is_available()
dev = torch.device('cuda' if use_gpu else 'cpu')
if use_gpu:
    torch.cuda.set_per_process_memory_fraction(1/8.42) # try to limit GPU usage for shared machines (there are 8 users, use slightly less)
    # ^ not sure this is actually working...
#######

print("Loading Dataset")

training_custom_dataloader, test_custom_dataloader = load_custom_dataset(BATCH_SIZE, dev, DATASET_TINY)
print(f"Batch size is {BATCH_SIZE} with {len(training_custom_dataloader)} training batches, and {len(test_custom_dataloader)} test batches.")


actual_seed = SEED
if SEED != 0:
    torch.manual_seed(SEED)
else:
    actual_seed = torch.seed()

print("Using seed", actual_seed)
# if you use random or np.random, fix their seeds too


###################
if DATASET_PREVIEW_IMAGES > 0:
    from matplotlib import pyplot as plt
    K = 10
    sets = [dl.__iter__().__next__() for dl in [
        training_custom_dataloader, test_custom_dataloader
    ]]
    for i in range(K):
        for r,(s, t) in enumerate(sets):
            plt.subplot(2, K, 1+i+r*K)
            plt.imshow(s[i,0,:,:])
            plt.title(f'{t[i]}: {classes[t[i]]}')
    plt.colorbar()
    plt.show()



print("Defining the Model (and loss)")

nn_loss = nn.CrossEntropyLoss()
Act1 = nn.ReLU
Act2 = nn.Tanh
model = nn.Sequential(
    # the input is a 1 channel image
    nn.BatchNorm2d(1),
    nn.Conv2d(1, 512, kernel_size=5, stride=3, padding=3), # here (32+2*3-5)//3+1=12
    nn.BatchNorm2d(512),
    nn.Dropout(0.6),
    Act1(),
    nn.Conv2d(512, 64, kernel_size=2, padding=4, dilation=3), # here 17
    nn.BatchNorm2d(64),
    Act1(),
    nn.Conv2d(64, 64, kernel_size=2), # here 17-1=16
    nn.BatchNorm2d(64),
    nn.Dropout(0.1),
    nn.MaxPool2d(2), # 8
    Act2(),
    nn.Conv2d(64, 256, kernel_size=4), # here 8-3=5
    nn.BatchNorm2d(256),
    nn.Dropout(0.1),
    Act2(),
    nn.Flatten(),
    nn.Dropout(0.5),
    nn.Linear(
        256 * 5**2, # computed from the conv and pool above, **2 bc height==width.
        len(classes)
    )
    # the output is len(classes)-sized logits
)
optimizer = torch.optim.Adam(model.parameters(),lr=LR)

if use_gpu:
    print("Using GPU")
    model.to(dev)
else:
    print("Using cpu")

if reload_autosave:
    print("Reloading model")
    savepoint = torch.load('autosave.pt')
    model.load_state_dict(savepoint['model'])
    optimizer.load_state_dict(savepoint['optim'])
    model.train()
    
timer = SimpleTimer()
for t in range(EPOCHS):
    timer.reset()
    if QUIET:
        more1 = dict()
        more2 = dict(mod=10000000)
    else:
        more1 = dict(loss_fn=nn_loss, label_end=timer.log('(time {ms}ms)'))
        more2 = dict()
    
    if t==0 and EVAL_INIT or t > 0 and t%EVAL_MOD == 0:
        evaluate_accuracy(training_custom_dataloader, model, "Evaluating on the training set", **more1)
        timer.reset()
        evaluate_accuracy(test_custom_dataloader, model, "Evaluating on the test set", **more1)
        timer.reset()
    
    rolling_loss = train_one_epoch(training_custom_dataloader, model, nn_loss, optimizer, f"epoch {t+1}", **more2)
    print(f"... average forward loss {rolling_loss:>8f}", timer.format('(epoch done in {s}sec)'))

    #torch.save({
    #        'model': model.state_dict(),
    #        'optim': optimizer.state_dict(),
    #        'model_str': str(model),
    #        'loss_str': str(nn_loss),
    #        }, 'autosave.pt')

    #model_scripted = torch.jit.script(model)
    #model_scripted.save('model_scripted.pt')
    