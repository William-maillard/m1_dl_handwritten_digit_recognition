#!/bin/bash

function sep() {
    echo "#### ### ## # $@" >> "$LOG"
}

LOG="trace-$(date --iso-8601=seconds | sed 's@[+].*@@g').log"

sep FILE "$LOG"
sep CMDLINE "$@"
sep GIT
{
    git rev-parse HEAD
    git status
    git diff BASE
} >> $LOG

sep TRAIN
echo "### #### #############################"
echo "LOG FILE $LOG"
echo "### #### #############################"
if [ -d /tmp/custom-pip ] ; then
    PYTHONPATH=/tmp/custom-pip python3 train.py DO-RUN "$@" | tee -a "$LOG"
else
    python3 train.py DO-RUN "$@" | tee -a "$LOG"
fi



